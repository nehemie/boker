## Organisation of the data

After the fieldwork was completed, raw data were exported to the
file `./uncleaned/boker_strupler_2012_unclean.tsv` and
`./uncleaned/boker_strupler_2013_unclean.tsv`. To exclude and
correct inconsistent results, typos, impossible combinations, clumsy
clicks and wrong answers. Strupler wrote the scripts in the folder
`./uncleaned/`. This first data processing allows the transition from
'unlceaned' raw data to primary analyzable data and organise it in
 4 main subsets (*boker_wter*, *boker_knw*, *boker_tvs*, *boker_suda*)

### 


## Description of the data

### Selection of samples

The method developed by Schoop requires the study of "large",
non-functional pottery assemblages  to have a sufficiently large
number of individuals and to avoid biased result in
quantitative analysis. 

### Data collection

The database was originally designed in German by Strupler. The
material was directly recorded in the database and each diagnostic
individual was entered into the database data thanks to a unique
identifier (id), which was also written in ink on the sherds. The term
individual refers to the statistical term i.e. one or more joint
fragments (During the fieldwork, time was always spent to find joins
in the pottery).
## Description

    The description is a rough tranlation of the appendix B from
    *Strupler *in press*, la ville basse*

This is a data set from the Bronze Age Pottery from Boğazköy with
4071 observations on the following 40 variables.

- **id** : Identifier of each observation (for example Bo73-46-8),
  composed of the three variables **year**, **context_number** and a
serial number, linked by a hyphen. This identifier was written on the
sherds with ink and as reference for the drawings.

- **year**: Year of excavation.

- **context**: Stratigraphical context (see dedicated file)

- **sherd_type**: Only diagnostic sherds have been taken into account
  and are classified between: complete profile, rim, base, decoration,
handle and wall. The terms are ordered in the order presented here and
exclusive. If a shard is a base and has a decoration, only the type
*base* is selected.

- **sherd_number**: Number of join fragments forming the individu

- **type**: Type of form (see *Topology*)

- **rim_diameter**: Diameter (in mm) of the rim

- **rim_eve**: Rim sherd as a percentage of the whole rim. The percentage
of the rim sherd was estimated using a rim chart divided into 2.5%
classes.

- **rim_thickness**: Thickness of the rim (in mm) [See *Schoop 2006,
  Dating*] For type *KRAD*, *SHAMRD*, *SUKER*, *SUKNIKS*, *TED*,
*TELD*, *TELK*.

- **rim_length**: Length of the rim (in mm) [See *Schoop 2006,
  Dating*]. For type *KRAD*, *SHAMRD*, *SUKER*, *SUKNIKS*, *TED*,
*TELD*, *TELK*.

- **rim_angle**: Angle of the opening (in degree) [See *Schoop 2006,
  Dating*]. For type *SUKER*

- **base_diameter**: Diameter (in mm) of the base

- **base_eve**: Base sherd as a percentage of the whole base. The percentage
of the base sherd was estimated using a rim chart divided into 2.5%
classes.

- **carination_angle**: Angle of carination [See *Schoop 2006, Dating*]

- **weight**: Weight (in gram)

- **refiring**: secondary (reducing) firing is (mostly) the result of
  using ceramics for firing. This variable therefore records a
refiring color change, which can be either: on the rim, outside or
inside.

- **slip_colour**: The external coating for closed vessels or the
  internal coating for open vases is always interpreted as slip,
without exception. It is often difficult to tell the difference
between a slip, an extra diluted clay coating and the usage of water
to produces a self-slip. Thus *slip* designates a global surface
treatment of the vase in opposition to the painting which is a local
treatment. If the vase has no special treatment, its surface is
considered as self-slip. Otherwise, the color of the slip has been
recorded (see [Colour Chart](colour-chart)). The possible modalities of the colors
of a slip were increased with the modalities *self-slip* and
*micaceous*,  a slip with golden reflections (from German
*Goldglimmerüberzug*)

- **slip_extent**: if a specific slip has been applied to the surface
  of the vase, then its extension is recorded using this variable: outside (only),
inside (only) inside and outside.

- **slip_colour_homogeneity**: Homogeneity of the slip colour (*not
  uniform* or *uniform*)

- **polish_finish**: Polishing is the step that gives a shiny
  appearance to the vase by rubbing with a tool. Polishing took place
when the outer surface of the vase had the consistency of leather with
a hard, rounded object. Most of the time, the tool marks are visible
and have a rather fine appearance, are documented according to their
orientation and their density.

- **polish_extent**: If the ceramic is polished, the polishing is most
  often distributed over the entire surface, exterior, interior or
both at once. If polishing is localized, it then focuses either on the
painting or on the rim of the vessel. The modalities of this variable
are:

- **painting_type**: Painting refers to any local application of
  colour. The types of paintings encountered at Boğazköy are rare and
not very varied. The painting is limited to the rim or from the rim to
the carination.

- **painting_colour**: see [Colour Chart](colour-chart)

- **decoration**: Surface treatments, other than the use of slip or
  paint, are very rare. Some sherds have been incised, others stamped,
in particular with the "royal sign".

- **fabric**: the typology of ceramic wares is based solely on
  inclusions. Colours, type of firing, surface treatments are not
considered as characteristics of the ceramic paste and have been
registered as technological criteria. I did not divide the inclusions
according to their nature, having no competence in this field. The
simplicity of this division allowed it to be quickly adopted by the
working group to ensure consistency of data. Only mica, easily enough
identifiable, was registered independently. See the charts
[*Frequency* and *sorting* below](size-form-and-sorting-of-inclusion).
The 4 main fabrics are:
  - Fine ware:
    - Inclusion colors: brown, gray black, gray white
    - Size of inclusions: from 0.5 to 1 mm
    - Frequency: 5%
    - Form: D2
    - Sorting: 4 or 5

  - Kitchenware:
    - Inclusion colors: brown, gray black, gray white
    - Size of inclusions: from 0.5 to 3 mm
    - Frequency: 30%
    - Form: A2
    - Sorting: 2

  - Standart ware:
    - Inclusion colours: brown, grey-black, grey-white, large calcite
      fragments
    - Size of inclusions: from 0.5 to 2 mm
    - Frequency: 20%
    - Form: B3
    - Sorting: 3

  - White ware:
    - Inclusion colours:
    - Size of inclusions:
    - Frequency:
    - Form:
    - Sorting: 3

- **fabric_mica**: proportion of mica inclusions

- **core_firing** The appearance of the core of a ceramic has been
  described according to Orton's diagram [figure], slightly modified
to add the cases of a reducing firing with a short oxidizing phase at
the end of firing (C1) and the opposite case, a reducing firing, but
an oxidized inner wall (C2).

- **core_texture**: aspect of the fresh break

- **core_out**: colour of the outer part of the core (see [Colour
  Chart](colour-chart))

- **core_middle**: colour in the middle of the core (see [Colour
  Chart](colour-chart))


- **core_in**: colour of the inner part of the core (see [Colour
  Chart](colour-chart))

- **core_aspect**: distribution of inclusions in the ceramic paste

- **core_hardness**: hardness of the core.

- **forming_technique**: Technique used to form the ceramic (*built*, *hand-formed* or *wheel-made*)

- **forming_traces**: construction traces. There are many traces of
  construction/forming between different parts of the same vessel. For
example, jars with pouring spouts are most often made in three parts:
the lower part, the body and the neck. Clay plate mounting is used
for vessels whose diameter and weight do not permit to be wheel made
such as storage jars with a capacity of more than 100 liters. This
variable records which different part can be identified if a
connection between part were visible

- **drawing**: presence or absence of a pencil drawing of the sherd

- **illustration**: presence or absence of a clean digital drawing.

- **date_modification**: Date of last modification of the record

- **author**: responsible of the dataset

- **subset**: a value determining subset:
    + *knw*: *Kesikkaya Nordwest*: In 2010 and 2011, Strupler worked
      on the ceramics of the Karum period from Kesi̇kkaya Nordwest. It
was expended in 2013 by Beckmann and Strupler with the pottery from
the 2013 excavation of a deep test-pist.
   + *wter*: *West-Terrasse*: publish in [Strupler, Ville basse]. Data
     collectdd by Egbers, Göçmez, Strupler and Wittmann
   + *tvs*: *Tal vor Sarıkale*: work in progress. Data collectdd by
     Egbers, Göçmez, Strupler and Wittmann
   + *suda*: *Südareal*: work in progress.  Data collectdd by Egbers,
     Göçmez, Strupler and Wittmann


### Colour chart

Colour help to determine the firing method, understand the
post-deposition processes or the nature of the clay. It is rare that
these are homogeneous for a whole vase. For example, the position of
the ceramic in the furnace during the firing process strongly
influences the colour of a ceramic. Munsell's colour code was not
used because it is too precise and heavy to handle. The attribution of
a color to a ceramic, even the most precise possible, reflects only
the coloring at a precise place of the vase, determined under
conditions that are not strictly identical (changing light). For these
reasons, a sample of 8 colours was created, a palette sufficiently
distinct so that the answers given by each collaborator are
homogeneous. A restriction to 8 colours limits the number of possible
theoretical combinations for each sherd to `8^3` possibilities, i.e.
512 combinations, which should cover the spectrum of the different
ancient cooking modes and make it possible to statistically process
the data to find those which are most frequent.

![color chart](boker_strupler_meta_core_colors_chart.svg)

### Size, Form and Sorting of inclusion

Charts used to determine the size, percentage and sorting of
inclusions of the ware.

![Firing core types, redraw and modified from C. Orton et al. 1993, Pottery
in Archaeology, 134 Fig. 11.1](boker_strupler_meta_core_firing_core.svg)

![Inclusion sorting chart, redraw from C. Orton 1993, Pottery in
Archaeology, Appendix, Fig. A.6](boker_strupler_meta_core_inclusion_selection.svg)

![Form of inclusion chart, redraw from Krumbein, William C. and Sloss,
Laurence L. 1956, Stratigraphy and Sedimentation, 111 Fig.
4-10](boker_strupler_meta_core_inclusion_shape.svg)

![Percentage Inclusion Estimation Chart, redraw from C. Orton, Pottery
Archaeology, 1993, Appendix, Fig.  4](boker_strupler_meta_core_inclusion_size.svg)


### References

 - **Krumbein & Sloss 1956**: W. C. Krumbein – L. L. Sloss,
   Stratigraphy and Sedimentation (San Francisco 1956)
 - **Orton *et al.* 1993**: Orton et al. 1993 C. Orton – P. Tyers – A.
   G. Vince, Pottery in Archaeology, Cambridge Manuals in Archaeology
(Cambridge 1993)
 - **Schoop 2006**:i U.-D. Schoop, Dating the Hittites with
   Statistics. Ten Pottery Assemblages from Boğazköy–Ḫattuša, in : D.
P.  Mielke − U.-D. Schoop – J. Seeher (éd.), Strukturierung und
Datierung in der hethitischen Archäologie, Byzas 4 (Istanbul 2006)
215-240
 - **Strupler _in press_**, La ville basse de Boğazköy au II^e^
   millénaire av. J.-C. Une étude de l'organisation urbaine de la
cité-État et de sa restructuration en capitale du royaume hittite

