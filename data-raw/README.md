# Origin of the data

## Folder: `boker_strupler`

These assemblages are provided by Néhémie Strupler and were created
between 2010 and 2013.

The data were collected in the scope of the PhD thesis of Néhémie
Strupler and were first described in the appendix. Citation :

> Strupler, N. La ville basse de Boğazköy au II^e^ millénaire av.
> J.-C. Une étude de l'organisation urbaine de la cité-État et de sa
> restructuration en capitale du royaume hittite, Appendix B, Thèse de
> doctorat de l'Université de Strasbourg, 2016

See the README file in `./boker_strupler/` folder for more information
on these data sets as-well as on the license.

## Folder: `boker_schoop`

These data are the values from the figures of the article

> U.-D. Schoop, ‘Dating the Hittites with Statistics. Ten Pottery
> Assemblages', in: Mielke, D. P.; Schoop, U.-D. & Seeher, J. (Eds.)
> _Strukturierung und Datierung in der hethitischen Archäologie_, Ege
> Yayınları, 2006, 215-240.

See the README file in `./boker_schoop/` folder for more information
on these data sets as-well as on the license.
