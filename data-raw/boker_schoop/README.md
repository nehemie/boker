README
------

## Copyright:

Copyright owner of the data is: U.-D. Schoop

License: unknown (aka _fair use_)

## Origin of the data

These data are the values from the figures of the article

> U.-D. Schoop, ‘Dating the Hittites with Statistics. Ten Pottery
> Assemblages', in: Mielke, D. P.; Schoop, U.-D. & Seeher, J. (Eds.)
> _Strukturierung und Datierung in der hethitischen Archäologie_, Ege
> Yayınları, 2006, 215-240.

These data weren't originally meant to be published as is but Schoop
graciously transmitted it to reproduce his plots.

## Description of the variables

 - *context*: name of the assemblage
 - *rim_diameter*: mean of diameter (in mm)
 - *rim_thickness*: Thickness of the rim (in mm)
 - *rim_length*: Length of the rim (in mm)
 - *rim_eve*: rim-equivalent of subset
 - *req_other*: rim-equivalent of all but the subset
 - *sum_req*: sum of rim-equivalent in the set
 - *n*:	number of element of subset
 - *n_other*: n of other elements in the set
 - *author*: responsible of the dataset

## Encoding

  - tsv
  - UTF-8
