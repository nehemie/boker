#' Frequency of two types of bowls or cooking pots
#'
#' This function plots a two frequency plot for flat bowls type F (SHAMRD) and
#' G (SUKER) and for cooking pot type C and D
#'
#' @param type a character. One of "bowls" or "cooking_pots"
#' @param context character. Context to be plot. One of context(boker)
#' @param position Number: A position to plot the selected context
#'
#' @import graphics
#' @importFrom stats xtabs
#'
#' @examples
#' schoop_double_freq(type = "bowls")
#' schoop_double_freq(type = "cooking_pots")
#' schoop_double_freq(type = "bowls", "Geb10", 2)
#' schoop_double_freq(type = "cooking_pots", "Geb13", 7)
#' @export
schoop_double_freq <- function(type = "bowls", context = NULL, position = 5){

  object <- boker::boker

  ifelse(type == "bowls",  schoop_data <- schoop_2bowls,
         schoop_data <- schoop_2cookings)

  # Set palette to two grey colors
  opar <- par(no.readonly = TRUE)
  opalette <- palette()
  palette(grey(c(0.95, 0.75)))
  on.exit(c(par(opar), palette(opalette)))


  if (is.null(context)) {
    par(xpd = TRUE)
    data_set <- expand_schoop_rim_eve(schoop_data)
    d <- prop.table(xtabs(~ data_set$type + data_set$context,
                          subset = NULL),  2) * 100
    r <- prop.table(xtabs(data_set$rim_eve ~ data_set$type + data_set$context,
                          subset = NULL), 2) * 100
    bp <- barplot(r[1, ], ylim = c(0, 100))
    points(bp, d[1, ])
    lines(bp, d[1, ])
    title( main = paste("Frequency curves of", levels(data_set$type)[1],
                        "against", levels(data_set$type)[2]),
           sub = "Schoop 2006, fig.7")
  } else {
    valid_context(context = context, object = object)
    valid_position(position = position)
    context_order  <- put_position(context = context, position = position)
    subdata_set <- object[object$context %in% context, ]

    data_set    <- merge(subdata_set,
                       expand_schoop_rim_eve(schoop_data), all = TRUE)
    data_set <- data_set[data_set$type %in% schoop_data$type, ]
    data_set$type <- relevel(data_set$type, ref = "TEKU")
    data_set$type <- droplevels(data_set$type)

#Legend
n_evaluated  <- nrow(subdata_set[subdata_set$type %in% levels(data_set$type), ])
n_total      <- nrow(data_set[data_set$type  %in% levels(data_set$type), ])
n_label      <- paste0("n(", context, ")=", n_evaluated, "  ",
                     "n=", n_total)
set_colour   <- append( rep(1, length(schoop_assemblage_names)),
               rep(2, length(context)), after = position ) #select2
# Make table
data_set$context <- factor(data_set$context, levels = context_order) # order

d               <- prop.table(xtabs(~ data_set$type +
                                    data_set$context,
                                    subset = NULL), 2) * 100

r               <- prop.table(xtabs(data_set$rim_eve ~
                                        data_set$type +
                                      data_set$context,
                                    subset = NULL), 2) * 100

# Plot Schoop2006-Fig6 -------------------------------------
bp              <- barplot(r[1, ],
                           ylim = c(0, 100),
                           axes = FALSE,
                           axisnames = TRUE,
                           las = 2,
                           cex.names = 0.7,
                           mgp = c(3, 0.3, 0),
                           col = set_colour)
title(paste("Frequency curves of", levels(data_set$type)[1],
                        "against", levels(data_set$type)[2]),
      line = -1,
      cex.main = 0.8)

text(x = 10,
     y = 90,
     labels = n_label,
     cex = 0.6)

my.axis <- paste(axTicks(2),
                c(rep("", length(axTicks(2)) - 1), " %"),
                sep = "")
axis(2,
     at = axTicks(2),
     labels = my.axis,
     cex.axis = 0.7,
     mgp = c(1, 0.5, 0),
     las = 1,
     tcl = -0.3)

lines(x = bp[-charmatch(context, context_order)],
      y = d[1, intersect(colnames(d), schoop_assemblage_names)],
      lty = "dotted",
      col = "black")

points(x = bp[-charmatch(context, context_order)],
       y = d[1, intersect(colnames(d), schoop_assemblage_names)],
       col = "black", pch = 1)

points(x = bp[charmatch(context, context_order)],
       y = d[1, setdiff(colnames(d), schoop_assemblage_names)],
       col = "black",
       pch = 1)
    }
}
