#' Diameter and rim
#'
#' This function produces
#'
#' @param context character. Context to be plot. One of context(boker)
#' @param type a character. One of "baking_plates" or "cooking_pots"
#' @param position Number: A position to plot the selected context
#'
#' @examples
#' schoop_diam_rim(type = "cooking_pots",
#'                 context = "Geb10",
#'                 position = 5
#'                            )
#' schoop_diam_rim(type = "baking_plates",
#'                 context = "Geb19",
#'                 position = 5
#'                            )
##importFrom
#'
#' @export
#'
schoop_diam_rim <- function(
                            type = "cooking_pots",
                            context = NULL,
                            position = 5
                            ){

  # Set palette to two grey colors
  opar <- par(no.readonly = TRUE)
  opalette <- palette()
  palette(grey(c(0.45, 0.85)))
  on.exit(c(par(opar), palette(opalette)))


  if (type == "cooking_pots"){
         data_mean <- schoop_ted_mean
         data_eve <- schoop_ted_eve
  }else{
         data_mean <- schoop_teld_mean
         data_eve <- schoop_teld_eve
  }

  data_mean$rim_dia_length_mean <-
    with(data_mean, rim_diameter_mean / rim_length_mean)
  data_eve$rim_dia_length_eve <-
    with(data_eve, rim_diameter_eve / rim_length_eve)

  context_order  <- schoop_assemblage_names_l

  if (!is.null(context)) {

    palette(grey(c(0.85, 0.45)))

    data_set <- boker::boker

    # Validity
    valid_context(context = context, object = data_set)
    valid_position_l(position = position)
    context_order  <-
      put_position_l(context = context, position = position)
    data_set <- data_set[data_set$context %in% context, ]
    # Shape data_set
    data_set <- data_set[, c("rim_diameter",
                             "rim_eve",
                             "rim_length",
                             "rim_thickness",
                             "type",
                             "context"), ]
    data_set <- data_set[data_set$type %in% data_mean$type, ]
    data_set <- droplevels(data_set)

    # Create 2 functions to compute mean and weighted mean
    # after removing na.rm
    mean_rm <- function(x) mean(x, na.rm = TRUE)
    weight_rm <- function(x, y) {
      nonandex <- !is.na(x + y)
      x <- x[nonandex]
      y <- y[nonandex]
      sum(x * y) / sum(y)
    }

    rim_diameter_mean <-
      tapply(data_set$rim_diameter, data_set$context, mean_rm)

    rim_thickness_mean <-
      tapply(data_set$rim_thickness, data_set$context, mean_rm)

    rim_length_mean <-
      tapply(data_set$rim_length, data_set$context, mean_rm)

    rim_diameter_eve <-
      cbind(by(data_set,
               data_set$context,
               function(x) weight_rm(x$rim_diameter, x$rim_eve)))

    rim_length_eve <-
      cbind(by(data_set,
               data_set$context,
               function(x) weight_rm(x$rim_length, x$rim_eve)))

    rim_thickness_eve <-
      cbind(by(data_set,
               data_set$context,
               function(x) weight_rm(x$rim_thickness, x$rim_eve)))

    rim_dia_length_mean <- rim_diameter_mean / rim_length_mean

    rim_dia_length_eve  <- rim_diameter_eve / rim_length_eve

    df_mean <- data.frame(rim_diameter_mean,
                          rim_thickness_mean,
                          rim_length_mean,
                          rim_dia_length_mean,
                          context = names(rim_diameter_mean),
                          type = levels(data_set$type) )

   data_mean <- rbind(data_mean, df_mean)
   data_mean$context <- factor(data_mean$context, context_order)

   df_eve <- data.frame(rim_diameter_eve,
                          rim_thickness_eve,
                          rim_length_eve,
                          rim_dia_length_eve,
                         context = unlist(dimnames(rim_diameter_eve)),
                          type = levels(data_set$type))

   data_eve <- rbind(data_eve, df_eve)
   data_eve$context <- factor(data_eve$context, context_order)
  }


  # Legend data
   n_total      <- 360
  if (is.null(context)) {
    text_legend  <- paste0("n = ", n_total, " (ca)")
  } else {
    n_evaluated  <- nrow(data_set[data_set$context %in% context, ]) - 9
    text_legend  <- paste0("n(", context, ")=", n_evaluated, ";  ",
                         "n(total)=", n_total, " (ca)")
   n_total       <-  n_total +  nrow(data_set)
  }

  par(mfcol = c(1, 3), xpd = TRUE)

  # calculate min and max for y
  lim_10percent <- function(x){
    dif <- max(x, na.rm = TRUE) - min(x, na.rm = TRUE)
    range(x, na.rm = TRUE) +
      c(-0.2 * dif, 0.2 * dif)
  }

  ## Plot diameter
   plot(data_eve$rim_diameter_eve,
     type = "n",
     axes = F,
     xlab = "",
     ylim = lim_10percent(data_eve$rim_diameter_eve),
     ylab = "",
     main = "")

  ### Legend
  title(main = paste("Diametre of cooking pots",
                    "\n",
                    text_legend),
       font.main = 6,
       cex.main = 0.8,
       line = 0)

  my.axis <- paste0(axTicks(2),
                   c("\n mm", rep("", length(axTicks(2)) - 1)))

  axis(2,
       at = axTicks(2),
       labels = my.axis,
       cex.axis = 0.7,
       mgp = c(1, 0.5, 0),
       las = 1,
       tcl = -0.3)

  axis(1,
       at = 1:length(context_order),
       labels = context_order,
       cex.axis = 0.7,
       mgp = c(1, 0.5, 0),
       tcl = -0.3,
       las = 2)

  legend(x = 1,
         y = max(data_eve$rim_diameter_eve),
         pch = c(16, 1),
         lty = c("solid", NA),
         legend = c("eve mean", "mean"),
         cex = 0.7,
         bty = "n")

  # Add lines: First rim eve
  lines( x = data_eve[data_eve$context %in%
                      schoop_assemblage_names, "context"],
         y = data_eve[data_eve$context %in%
                      schoop_assemblage_names, "rim_diameter_eve"],
         col = palette()[1])

  points(x = data_eve[data_eve$context %in%
                      schoop_assemblage_names, "context"],
         y = data_eve[data_eve$context %in%
                      schoop_assemblage_names, "rim_diameter_eve"],
        pch = 16,
        col = palette()[1])

  points(x = data_eve[data_eve$context %in%
                      context, "context"],
         y = data_eve[data_eve$context %in%
                      context, "rim_diameter_eve"],
         pch = 21,
         bg = palette()[2])

  # Add points from count
  points(x = data_mean[data_mean$context %in%
                      schoop_assemblage_names, "context"],
         y = data_mean[data_mean$context %in%
                      schoop_assemblage_names, "rim_diameter_mean"],
         pch = 1,
         col = palette()[1])
  points(x = data_mean[data_mean$context %in%
                      context, "context"],
         y = data_mean[data_mean$context %in%
                      context, "rim_diameter_mean"],
        pch = 21,
        col = palette()[2])



# Plot: TED-RLRB -- Schoop2006-Fig12A -------------------------------

  plot(c(data_eve$rim_thickness_eve),
       type = "n",
       axes = F,
       xlab = "",
       ylab = "",
       main = "",
       ylim = lim_10percent(c(data_eve$rim_thickness_eve,
                              data_eve$rim_length_eve))
       ) # Span of col much better

  ### Legend
  title(main = paste("Rim length and thickness",
                    "\n",
                    text_legend),
       font.main = 6,
       cex.main = 0.8,
       line = 0)


  my.axis <- paste0(axTicks(2),
                    c("\n mm", rep("", length(axTicks(2)) - 1)))
  axis(2,
       at = axTicks(2),
       labels = my.axis,
       cex.axis = 0.7,
       mgp = c(1, 0.5, 0),
       las = 1,
       tcl = -0.3)

  axis(1,
       at = 1:length(context_order),
       labels = context_order,
       cex.axis = 0.7,
       mgp = c(1, 0.5, 0),
       tcl = -0.3,
       las = 2)

  legend(x = 1,
         y = 26,
         pch = c(16, 17, 1, 2),
         lty = c("dashed", "dotted", NA, NA),
         legend = c("length (eve mean)", "thickness (eve mean)",
                    "length (mean)", "thickness (mean)"),
         cex = 0.7,
         bty = "n")

  #  First rim length eve
  lines(x = data_eve[data_eve$context
                     %in% schoop_assemblage_names, "context"],
        y = data_eve[data_eve$context
                     %in% schoop_assemblage_names, "rim_length_eve"],
        col = palette()[1],
        lty = "dashed")
  points(x = data_eve[data_eve$context
                      %in% schoop_assemblage_names, "context"],
         y = data_eve[data_eve$context
                      %in% schoop_assemblage_names, "rim_length_eve"],
        pch = 16,
        col = palette()[1])
  points(x = data_eve[data_eve$context %in% context, "context"],
         y = data_eve[data_eve$context %in% context, "rim_length_eve"],
        pch = 21,
        bg = palette()[2])

  #  Second rim length count
  points(x = data_mean[data_mean$context
                      %in% schoop_assemblage_names, "context"],
         y = data_mean[data_mean$context
                      %in% schoop_assemblage_names, "rim_length_mean"],
        pch = 1,
        col = palette()[1])
  points(x = data_mean[data_mean$context %in% context, "context"],
         y = data_mean[data_mean$context %in% context, "rim_length_mean"],
        pch = 1,
        col = palette()[2])

  #  Third rim thickness eve
  lines(x = data_eve[data_eve$context
                     %in% schoop_assemblage_names, "context"],
        y = data_eve[data_eve$context
                     %in% schoop_assemblage_names, "rim_thickness_eve"],
        col = palette()[1],
        lty = "dotted")

  points(x = data_eve[data_eve$context
                      %in% schoop_assemblage_names, "context"],
         y = data_eve[data_eve$context
                      %in% schoop_assemblage_names, "rim_thickness_eve"],
        pch = 17,
        col = palette()[1])
  points(x = data_eve[data_eve$context
                      %in% context, "context"],
         y = data_eve[data_eve$context
                      %in% context, "rim_thickness_eve"],
        pch = 24,
        bg = palette()[2])

  # Forth rim thickness count
  points(x = data_mean[data_mean$context
                       %in% schoop_assemblage_names, "context"],
         y = data_mean[data_mean$context
                       %in% schoop_assemblage_names, "rim_thickness_mean"],
         pch = 2,
         col = palette()[1])
  points(x = data_mean[data_mean$context %in% context, "context"],
         y = data_mean[data_mean$context %in% context, "rim_thickness_mean"],
        pch = 2,
        col =  palette()[2])




  # Plot: TED-Diameter / Rim length --Schoop2006-Fig12B --------

  plot(data_eve$rim_dia_length_eve,
       type = "n",
       axes = F,
       xlab = "",
       ylab = "",
       main = "",
       ylim =  lim_10percent(data_eve$rim_dia_length_eve)
       )

  title(main = paste("Rim diameter / rim length",
                    "\n",
                    text_legend),
        font.main = 6,
        cex.main = 0.8,
        line = 0)

  my.axis <- paste0(axTicks(2),
                    c("\n index", rep("", length(axTicks(2)) - 1)))
  axis(2,
       at = axTicks(2),
       labels = my.axis,
       cex.axis = 0.7,
       mgp = c(1, 0.5, 0),
       las = 1,
       tcl = -0.3)

  axis(1,
       at = 1:length(context_order),
       labels = context_order,
       cex.axis = 0.7,
       mgp = c(1, 0.5, 0),
       tcl = -0.3,
       las = 2)

  legend(x = 9,
         y = 13.5,
         pch = c(16, 1),
         lty = c("dotdash", NA),
         legend = c("eve mean", "mean"),
         cex = 0.7,
         bty = "n")

  #  First index and rim-eve
  lines(x = data_eve[data_eve$context
                     %in% schoop_assemblage_names, "context"],
        y = data_eve[data_eve$context
                     %in% schoop_assemblage_names, "rim_dia_length_eve"],
        col =  palette()[1],
        lty = "dotdash")
  points(x = data_eve[data_eve$context
                      %in% schoop_assemblage_names, "context"],
         y = data_eve[data_eve$context
                      %in% schoop_assemblage_names, "rim_dia_length_eve"],
        pch = 16,
        col =  palette()[1])
  points(x = data_eve[data_eve$context %in% context, "context"],
         y = data_eve[data_eve$context %in% context, "rim_dia_length_eve"],
        pch = 21,
        bg =  palette()[2])

  #  Second rim length and count
  points(x = data_mean[data_mean$context
                      %in% schoop_assemblage_names, "context"],
         y = data_mean[data_mean$context
                      %in% schoop_assemblage_names, "rim_dia_length_mean"],
        pch = 1,
         col = palette()[1])
  points(x = data_mean[data_mean$context %in% context, "context"],
         y = data_mean[data_mean$context %in% context, "rim_dia_length_mean"],
        pch = 21,
        col =  palette()[2])
}
