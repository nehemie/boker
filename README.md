# Rpackage boker

`boker` will be a R package to facilitate the statistical analysis of
hittite pottery from Boğazköy after the method developed by Ulf
Schoop.

This repository is a **work in progress** and not intended for further
use.
